package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String DB_PROPERTIES = "app.properties";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}

		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.FIND_ALL_USERS)) {
			List<User> userList = new ArrayList<>();
			ResultSet resultSet = statement.executeQuery();

			while(resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));

				userList.add(user);
			}

			return userList;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.INSERT_USER)) {
			statement.setString(1, user.getLogin());

			statement.execute();

			user.setId(getUser(user.getLogin()).getId());
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = createConnection();

		boolean result = Arrays.stream(users).allMatch(user -> {
			try (PreparedStatement statement = connection.prepareStatement(SqlQueries.DELETE_USER_BY_LOGIN)) {
				statement.setString(1, user.getLogin());

				statement.execute();

				return true;
			} catch (SQLException e) {
				return false;
			}
		});

		closeConnection(connection);

		return result;
	}

	public User getUser(String login) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.GET_USER)) {
			User user = User.createUser(login);
			statement.setString(1, login);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				user.setId(resultSet.getInt("id"));
			}

			return user;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public Team getTeam(String name) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.GET_TEAM)) {
			Team team = Team.createTeam(name);
			statement.setString(1, name);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				team.setId(resultSet.getInt("id"));
			}

			return team;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.FIND_ALL_TEAMS)) {
			List<Team> teamList = new ArrayList<>();
			ResultSet resultSet = statement.executeQuery();

			while(resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));

				teamList.add(team);
			}

			return teamList;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.INSERT_TEAM)) {
			statement.setString(1, team.getName());

			statement.execute();

			team.setId(getTeam(team.getName()).getId());

			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = createConnection();
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}

		boolean result = Arrays.stream(teams).allMatch(team -> {
			try {
				return setTeamForUser(user, team, connection);
			} catch (SQLException | DBException e) {
				return false;
			}
		});

		try {
			if (result) {
				connection.commit();
				closeConnection(connection);
				return true;
			} else {
				connection.rollback();
				closeConnection(connection);
				throw new DBException();
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	private boolean setTeamForUser(User user, Team team, Connection connection) throws SQLException, DBException {
		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.SET_TEAM_FOR_USER)) {
			int userId = getUser(user.getLogin()).getId();
			statement.setInt(1, userId);
			statement.setInt(2, team.getId());

			statement.execute();
			return true;
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.GET_USER_TEAMS)) {
			List<Team> teamList = new ArrayList<>();
			statement.setString(1, user.getLogin());

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("team_id"));
				team.setName(resultSet.getString("name"));

				teamList.add(team);
			}

			return teamList;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.DELETE_TEAM_BY_NAME)) {
			statement.setString(1, team.getName());

			return statement.execute();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = createConnection();

		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.UPDATE_TEAM_NAME_BY_ID)) {
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());

			return statement.execute();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			closeConnection(connection);
		}
	}

	private static Connection createConnection() throws DBException {
		Connection connection;

		try (FileReader reader = new FileReader(DB_PROPERTIES)) {
			Properties properties = new Properties();
			properties.load(reader);

			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (SQLException | IOException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}

		return connection;
	}

	private void closeConnection(Connection connection) throws DBException {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

}

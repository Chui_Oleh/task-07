package com.epam.rd.java.basic.task7.db.entity;

public class SqlQueries {

    public static String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";

    public static String GET_USER = "SELECT * FROM users WHERE login = ?";

    public static String DELETE_USER_BY_LOGIN = "DELETE FROM users WHERE login = ?";

    public static String FIND_ALL_USERS = "SELECT * FROM users";

    public static String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";

    public static String GET_TEAM = "SELECT * FROM teams WHERE name = ?";

    public static String DELETE_TEAM_BY_NAME = "DELETE FROM teams WHERE name = ?";

    public static String UPDATE_TEAM_NAME_BY_ID = "UPDATE teams SET name = ? WHERE id = ?";

    public static String FIND_ALL_TEAMS = "SELECT * FROM teams";

    public static String SET_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";

    public static String GET_USER_TEAMS = "SELECT * FROM users_teams " +
            "JOIN teams t ON users_teams.team_id = t.id " +
            "JOIN users u ON users_teams.user_id = u.id " +
            "WHERE login = ?";

    private SqlQueries() {

    }

}

package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {

	public DBException() {
		super();
	}

	public DBException(String message, Throwable cause) {
		super(message, cause);
	}

}
